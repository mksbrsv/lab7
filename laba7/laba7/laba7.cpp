﻿#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "windows.h"
#include "conio.h"
#define MAX 1000
// Блок объявления функций
int EnterInt(int leftBorder, int rightBorder);
double EnterDouble(int leftBorder, int rightBorder);
void IntArray(int array[], int sizeArray);
void DoubleArray(double array[], int sizeArray);
void GenIntArray(int array[], int sizeArray);
void GenDoubleArray(double array[], int sizeArray);

int SearchMaxInt(int array[], int sizeArray);
double SearchMaxDouble(double array[], int sizeArray);
int SearchMinInt(int array[], int sizeArray);
double SearchMinDouble(double array[], int sizeArray);
int SearchNumberInt(int array[], int sizeArray);
int SearchNumberDouble(double array[], int sizeArray);
void SortInt(int array[], int sizeArray);
void SortDouble(double array[], int sizeArray);

int Menu(int switcher); //Функция вывода главного меню

int main()
{
	setlocale(LC_CTYPE, "Russian");
	int size, choose, left, right, exit;
	int numberInt, arrayInt[MAX], numberPoiskInt, minInt, maxInt;
	double numberDouble, arrayDouble[MAX], numberPoiskDouble, minDouble, maxDouble;
	srand((int)(time(0)));
	printf("\n  НАЧАЛО РАБОТЫ ПРОГРАММЫ\n\n");
	while (1) {
		printf("Вам нужно задать размер массива?\n1 - Да\n2 - Нет\nВвод: ");
		scanf_s("%d", &choose);
		if (choose == 1) {
			do {
				printf("Размер массива от 1 до %d\n", MAX);
				printf("Введите реальный размер массива: ");
				scanf_s("%d", &size);
			} while ((size < 1) && (size > MAX));
		}
		choose = Menu(1);
		switch (choose) {
		case 1: printf("Введите левую и правую границы: ");
			scanf_s("%d %d", &left, &right); numberInt = EnterInt(left, right); break;
		case 2: printf("Введите левую и правую границы: ");
			scanf_s("%d %d", &left, &right); numberDouble = EnterDouble(left, right); break;
		case 3:	IntArray(arrayInt, size); break;
		case 4: DoubleArray(arrayDouble, size); break;
		case 5: GenIntArray(arrayInt, size); break;
		case 6: GenDoubleArray(arrayDouble, size); break;
		case 7: maxInt = SearchMaxInt(arrayInt, size); printf("Результат работы: %d", maxInt); break;
		case 8: maxDouble = SearchMaxDouble(arrayDouble, size); printf("Результат работы: %lf", maxDouble); break;
		case 9: minInt = SearchMinInt(arrayInt, size); printf("Результат работы: %d", minInt); break;
		case 10: minDouble = SearchMinDouble(arrayDouble, size); printf("Результат работы: %lf", minDouble); break;
		case 11: numberPoiskInt = SearchNumberInt(arrayInt, size); printf("Результат работы: %d", numberPoiskInt); break;
		case 12: numberPoiskDouble = SearchNumberDouble(arrayDouble, size); printf("Результат работы: %lf", numberPoiskDouble); break;
		case 13: SortInt(arrayInt, size); break;
		case 14: SortDouble(arrayDouble, size); break;
		}
		getchar();
		getchar();
		system("cls");
		if (choose == 15) {
			system("cls");
			break;
		}
		if ((choose != 1) && (choose != 2) && (choose != 3) && (choose != 4 && (choose != 5) && (choose != 6) && (choose != 7) && (choose != 8) && (choose != 9) && (choose != 10) &&
			(choose != 11) && (choose != 12) && (choose != 13) && (choose != 14) && (choose != 5))) {
				system("cls");
				printf("Error!!! - Ошибка ввода\n");
				printf("Хотите продолжить?\nEnter - Да\nEsc - выйти");
				exit = _getch();
				if (exit == 13) {
					system("cls");
					continue;
				}
				if (exit == 27)
					system("cls");
					break;
		}
	}
}

int EnterInt(int leftBorder, int rightBorder) {
	int a = 0;
	do {
		printf("Введите целое число: ");
		scanf_s("%d", &a);
	} while ((a < leftBorder) || (a > rightBorder));
	return a;
}

double EnterDouble(int leftBorder, int rightBorder) {
	double a = 0;
	do {
		printf("Введите действительное число: ");
		scanf_s("%lf", &a);
	} while ((a < leftBorder) || (a > rightBorder));
	return a;
}

void IntArray(int array[], int sizeArray) {
	int choose;
	printf("1 - Заполнить массив\n2 - Вывести массив\nВвод: ");
	scanf_s("%d", &choose);
	if (choose == 1) {
		printf("Заполните массив\n");
		for (int i = 0; i < sizeArray; i++) {
			printf("[%d] = ", i);
			scanf_s("%d", &array[i]);
		}
	}
	if (choose == 2) {
		printf("[");
		for (int i = 0; i < sizeArray; i++) {
			printf("%d ", array[i]);
		}
		printf("]\n");
	}
	return;
}

void DoubleArray(double array[], int sizeArray) {
	int choose;
	printf("1 - Заполнить массив\n2 - Вывести массив\nВвод: ");
	scanf_s("%d", &choose);
	if (choose == 1) {
		printf("Заполните массив\n");
		for (int i = 0; i < sizeArray; i++) {
			printf("[%d] = ", i);
			scanf_s("%lf", &array[i]);
		}
	}
	if (choose == 2) {
		printf("[");
		for (int i = 0; i < sizeArray; i++) {
			printf("%lf ", array[i]);
		}
		printf("]\n");
	}
	return;
}

void GenIntArray(int array[], int sizeArray) {
	for (int i = 0; i < sizeArray; i++) {
		array[i] = rand();
	}
	return;
}

void GenDoubleArray(double array[], int sizeArray) {
	for (int i = 0; i < sizeArray; i++) {
		array[i] = rand() / 1000;
	}
	return;
}

int SearchMaxInt(int array[], int sizeArray) {
	int max = MININT32;
	for (int i = 1; i < sizeArray; i++) {
		if (array[i] > max)
			max = array[i];
	}
	return max;
}

double SearchMaxDouble(double array[], int sizeArray) {
	double max = 0;
	for (int i = 1; i < sizeArray; i++) {
		if (array[i] > max)
			max = array[i];
	}
	return max;
}

int SearchMinInt(int array[], int sizeArray) {
	int min = MAXINT32;
	for (int i = 1; i < sizeArray; i++) {
		if (array[i] < min)
			min = array[i];
	}
	return min;
}


double SearchMinDouble(double array[], int sizeArray) {
	double min = MAXLONG;
	for (int i = 1; i < sizeArray; i++) {
		if (array[i] < min)
			min = array[i];
	}
	return min;
}

int SearchNumberInt(int array[], int sizeArray) {
	int number;
	int pos = 0;
	printf("Введите число, которое нужно найти: ");
	scanf_s("%d", &number);
	int i = 0;
	array[sizeArray] = number; 
	while (array[i] != number) {
		i++;
		if (i == sizeArray)
			pos = -1;
		else
			pos = i;
	}
	return pos;
}

int SearchNumberDouble(double array[], int sizeArray) {
	double number;
	int pos = 0;
	printf("Введите число, которое нужно найти: ");
	scanf_s("%lf", &number);
	int i = 0;
	array[sizeArray] = number;
	while (array[i] != number) {
		i++;
		if (i == sizeArray)
			pos = -1;
		else
			pos = i;
	}
	return pos;
}

void SortInt(int array[], int sizeArray) {
	int tmp = 0;
	for (int i = 0; i < sizeArray - 1; i++) {
		for (int j = i + 1; j < sizeArray; j++) {
			if (array[j] < array[i]) {
				tmp = array[j];
				array[j] = array[i];
				array[i] = tmp;
			}
		}
	}
	printf("[");
	for (int i = 0; i < sizeArray; i++) {
		printf("%d ", array[i]);
	}
	printf("]\n");
	return;
}

void SortDouble(double array[], int sizeArray) {
	double tmp = 0;
	for (int i = 0; i < sizeArray - 1; i++) {
		for (int j = i + 1; j < sizeArray; j++) {
			if (array[j] < array[i]) {
				tmp = array[j];
				array[j] = array[i];
				array[i] = tmp;
			}
		}
	}
	printf("[");
	for (int i = 0; i < sizeArray; i++) {
		printf("%lf ", array[i]);
	}
	printf("]\n");
	return;
}

int Menu(int switcher) {
	system("cls");
	switch (switcher) {
	case 1: printf("1 - Ввод целого числа\n");
	case 2: printf("2 - Ввод действительного числа\n");
	case 3: printf("3 - Ввод/вывод целочисленного массива\n");
	case 4: printf("4 - Ввод/вывод действительного массива\n");
	case 5: printf("5 - Генерация значений целого массива\n");
	case 6: printf("6 - Генерация значений действительного массива\n");
	case 7: printf("7 - Поиск номера максимального значения целочисленного массива\n");
	case 8: printf("8 - Поиск номера максимального значения действительного массива\n");
	case 9: printf("9 - Поиск номера минималього значения челочисленного массива\n");
	case 10: printf("10 - Поиск номера минимального значения действительного массива\n");
	case 11: printf("11 - Поиск номера заданного элемента челочисленного массива\n");
	case 12: printf("12 - Поиск номера заданного элемента действительного массива\n");
	case 13: printf("13 - Сортировка целочисленного массива\n");
	case 14: printf("14 - Сортировка действительного массива\n");
	case 15: printf("15 - Выход\n");
	default: break;
	}
	printf("Введите номер: ");
	scanf_s("%d", &switcher);
	switch (switcher) {
	case 1: return 1; break;
	case 2: return 2; break;
	case 3: return 3; break;
	case 4: return 4; break;
	case 5: return 5; break;
	case 6: return 6; break;
	case 7: return 7; break;
	case 8: return 8; break;
	case 9: return 9; break;
	case 10: return 10; break;
	case 11: return 11; break;
	case 12: return 12; break;
	case 13: return 13; break;
	case 14: return 14; break;
	case 15: return 15; break;
	}
}